console.log("prueba");
var personas = [];
var persona1 = {};
persona1.nombre = 'javier';
persona1.edad = 19;
persona1.estatura = 1.80;
persona1.carro = false;

var persona2 = {};
persona2.nombre = 'daniel';
persona2.edad = 20;
persona2.estatura = 1.80;
persona2.carro = false;

var persona3 = {};
persona3.nombre = 'edson';
persona3.edad = 21;
persona3.estatura = 1.80;
persona3.carro = false;

var persona4 = {};
persona4.nombre = 'carlos';
persona4.edad = 22;
persona4.estatura = 1.80;
persona4.carro = false;

var persona5 = {};
persona5.nombre = 'diana';
persona5.edad = 23;
persona5.estatura = 1.80;
persona5.carro = false;

var persona6 = {};
persona6.nombre = 'alejandro';
persona6.edad = 24;
persona6.estatura = 1.80;
persona6.carro = false;

personas.push(persona1);
personas.push(persona2);
personas.push(persona3);
personas.push(persona4);
personas.push(persona5);
personas.push(persona6);
console.log(personas.length);
console.log(personas);

personas.forEach(function(element, index) {
    console.log((index + 1) + '.- ' + element.nombre);
    console.log((index + 1) + '.- ' + element.edad);
    console.log((index + 1) + '.- ' + element.estatura);
    console.log((index + 1) + '.- ' + element.carro);
});